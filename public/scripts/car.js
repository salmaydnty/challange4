class Car extends Component {
  static list = [];

  static init(cars) {
    this.list = cars.map((car) => new this(car));
  }

  constructor(props) {
    super(props);
    let {
      id,
      plate,
      manufacture,
      model,
      image,
      rentPerDay,
      capacity,
      description,
      transmission,
      available,
      type,
      year,
      options,
      specs,
      availableAt,
    } = props;
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }
  render() {
    return `
      <div class ="col-md-4 my-2">
      <div class="card">
        <div class="card-body h-50">
            <div class="car_image" style="display: flex; justify-content: center; margin-bottom: 30px;">
                <img src="${this.image}" style="width: 100%; height: 17vw; border-radius:2px;">
            </div>
          <h6 style="font-weight: bold;" class="card-title">
              ${this.manufacture} / ${this.model}
          </h6>
          <h5 style="font-weight: bold;">
              Rp. ${this.rentPerDay} / hari
          </h5>
         
          <table style="height: 110px; margin-bottom: 20px;">
              <tr>
                  
                  <td class="spesifikasi">${this.capacity} orang</td>
              </tr>
              <tr>
               
                <td class="spesifikasi">${this.transmission}</td>
            <tr>
                
                <td class="spesifikasi">Tahun ${this.year}</td>
            </tr>
          </table>
          <div class="d-grid">
              <a type="button" href="#" class="btn btn-success btn-block" 
              style="background-color: #5CB85F; border-color: #5CB85F;">Pilih mobil</a>
          </div>
        </div>
      </div>
  </div>
      `;
  }
}
